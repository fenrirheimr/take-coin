import { defineStore } from 'pinia'
import { passportStore } from './passport'
import { BACKEND, withAuthorization } from '@/remotes'

export const userStore = defineStore('user', {
  namespaced: true,
  state: () => {
    return {
      loading: true,
      loaded: false,
      user: {},
    }
  },
  getters: {
    getUserData(state) {
      return state.user
    },
    isLoaded(state) {
      return state.loaded
    },
    getCoinsValue(state) {
      console.log('user getCoinsValue', state.user.balance_personal)
      return state.user?.balance_personal
    },
    getToken() {
      console.log('1', passportStore()?.getAuthData)
      return passportStore()?.getAuthData?.access_token
    },
  },
  actions: {
    async userData(userId) {
      try {
        const token = this.getToken

        console.log('>>>', token)

        const { data } = await BACKEND.get('/api/user-data', withAuthorization(token, {
          params: {
            user_id: userId,
          },
        }))

        const count = JSON.parse(localStorage.getItem('balancePersonal') || 0)

        this.user = {...data}
        this.setIsLoaded()
      } catch (e) {
        console.log('e', e)
      }
    },
    setIsLoaded() {
      this.loaded = true
    }
  }
})
